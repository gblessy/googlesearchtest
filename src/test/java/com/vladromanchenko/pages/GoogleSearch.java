package com.vladromanchenko.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.junit.Assert.assertEquals;

/**
 * Created by v.romanchenko on 5/9/2016.
 */
public class GoogleSearch {

    public static ElementsCollection results = $$(".srg>.g");

    public static void followNthLink(int index) {
        results.get(index).$(".r a").click();
    }

    public static void assertCountResults(int count) {
        results.shouldHaveSize(count);
    }

    public static void search(String searchText) {
       $(by("name","q")).setValue(searchText).pressEnter();
    }

    public static void assertNthtResult(int index, String expectedText) {
        results.get(index).shouldHave(text(expectedText));
    }
}