package com.vladromanchenko;

import org.junit.Test;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.url;
import static com.vladromanchenko.pages.GoogleSearch.*;
import static org.junit.Assert.assertEquals;

/**
 * Created by v.romanchenko on 5/4/2016.
 */
public class GoogleSearchTest {

    @Test
    public void testSearchAndFollowLink() {

        open("http://google.com/ncr");
        search("Selenium automates browsers");
        assertCountResults(10);
        assertNthtResult(0, "Selenium automates browsers");
        followNthLink(0);
        $("#header").shouldBe(visible);
        assertEquals(url().toString(), "http://www.seleniumhq.org/");
    }
}
